import React    from 'react';
import MTR      from './MuwieriTableRow';
import MR       from '../helpers/MuwieriRequests';

const MuwieriTable = (props) => {
    const { helpathData } = props;
    const Link = React.createRef();
    const getExcel = async () => {
        const {status, data} = await MR.getHelpathExcel({host: props.url});

        if(Boolean(status) && status === 200){
            Link.current.href = URL.createObjectURL(new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"}));
            Link.current.download = `helpath-patients-${Date.now()}.xlsx`;
            await Link.current.click();
            Link.current.href = "#";
        }
        return ;
    }
    if(helpathData.length === 0){
        return (
            <div className="mb-2">
                <h2 className="text-3xl font-bold text-center">No se encontaron pacientes con acceso a HelPath asignados.</h2>
                <button className="mx-auto my-0" onClick={() => {window.location.reload()}}>Cambiar de usuario</button>
            </div>
        )
    }
    
    return (
        <>
            <div className="flex justify-between align-middle mb-2">
                <h2 className="text-3xl font-bold">Listado de pacientes que usan HelPath</h2>
                <button className="m-0" onClick={() => {getExcel()}}>Descargar Excel</button>
            </div>
            <div className="table-container">
                <table>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Edad</th>
                            <th>Sexo</th>
                            <th>Estado Civil</th>
                            <th>Centro de Salud</th>
                            <th>Conducta Suicida</th>
                            <th>Fecha de asignación</th>
                            <th>No. Sesiones</th>
                            <th>No. Alertas</th>
                            <th>Última alerta</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {helpathData.map( row => <MTR key={`${row.trat_id}-${row.id}`} data={ row }/>)}
                    </tbody>
                </table>
            </div>
            <button className="mx-auto my-0" onClick={() => {window.location.reload()}}>Cambiar de usuario</button>
            <a ref={Link} href="#" style={{display: 'none'}}></a>
        </>
    )
};

export default MuwieriTable;
