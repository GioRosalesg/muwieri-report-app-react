import { useState }   from 'react';
import Swal           from 'sweetalert2';
import MR             from '../helpers/MuwieriRequests';
import Logo           from '../helpers/Logo';


const SetURL = (props) => {
    const handleSubmit = async (e) => {
        e.preventDefault();

        console.log("Making fetch request...");
        const resp = await MR.checkAPIURL(URL);

        if(resp){
            Swal.fire({
                title: "Conexión exitosa",
                text: "Se usará este servidor Muwieri para hacer las peticiones pertinentes.",
                icon: "success"
            });
            localStorage.setItem("muwieriURL", URL);
            props.updateURL(URL);
        }else{
            Swal.fire({
                title: "Error",
                text: "El servidor de Muwieri no accesible.",
                icon: "error",
                confirmButtonText: "Entendido"
            });
            setURL("");
        }

        return resp;
    };
    const [URL, setURL] = useState(props.url);
    
    return (
        <div className="w-full h-full flex justify-center">
            <form onSubmit={handleSubmit}>
                <Logo />
                <h3 className="text-3xl font-bold text-gray-600 mb-4 block">Ingresa la URL de la instancia de Muwieri*</h3>
                <input 
                    type="text" 
                    value={URL} 
                    pattern="https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)" 
                    onChange={({target}) => {setURL(target.value)}} 
                    placeholder="Ingrese la URL de conexión" 
                    minLength="10" 
                    required 
                />
                <small>Requerido</small>
                <button>Verificar URL</button>
            </form>
        </div>
    )

};

export default SetURL;