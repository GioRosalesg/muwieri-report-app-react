import {useState}   from 'react';
import MR           from '../helpers/MuwieriRequests';
import Swal         from 'sweetalert2';
import Logo         from '../helpers/Logo';

const Login = (props) => {
    const   [email, setEmail]   = useState(""),
            [pass, setPass]     = useState(""),
    /*
        Esta función deberá de hacer la validación de las credenciales por primera vez,
        además de almacenar la respuesta de la API de Muwieri (los datos de los pacientes)
        en la prop helpath-data.

        Los datos de inicio de sesión se almacenarán en la memoria de Session storage
        por temas de seguridad ya que se eliminarán una vez que se cierre la pestaña.
    */
            submit = async (e) => {
                e.preventDefault();

                // Hacer comprobación de datos
                const resp = await MR.getHelpathData({host: props.url, user: email, pass: pass});

                if(Boolean(resp.status) && resp.status === 200){
                    const {user: {first_name, email}, data} = resp;

                    Swal.fire({
                        title: "Yeah!",
                        text: "Inicio de sesión correcto!",
                        icon: "success"
                    });
                    props.handleLogin({user: {email, name: first_name}}, data);
                }else{
                    Swal.fire({
                        title: "Error",
                        text: "Inicio de sesión fallido.",
                        icon: "error"
                    });
                    setEmail("");
                    setPass("");
                }
            };

    return (
        <form className="login" onSubmit={submit}>
            <Logo />
            <h1 className="text-3xl mt-4">Inicio de Sesión</h1>
            <div className="form-group">
                <label htmlFor="user-email">Email</label>
                <input id="user-email" type="email" value={email} onChange={({target}) => { setEmail(target.value)} } required autoFocus />
                <small>Requerido</small>
            </div>
            <div className="form-group">
                <label htmlFor="user-password">Contraseña</label>
                <input id="user-password" type="password" value={pass} onChange={({target}) => { setPass(target.value)} } required />
                <small>Requerido</small>
            </div>

            <button>Confirmar credenciales</button>
        </form>
    )
};

export default Login;