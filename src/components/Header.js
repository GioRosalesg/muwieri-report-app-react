import Swal from 'sweetalert2';

const Header = (props) => {
    const {url, updateURL, login} = props;

    const deleteCurrentServerData = (e) => {
        e.preventDefault();

        Swal.fire({
            title: "¿Está seguro?",
            text: "Se borrarán los datos de inicio de sesión así como la dirección del servidor que actualmente se está usando.",
            icon: "question",
            showDenyButton: true,
            denyButtonText: "Cancelar",
            confirmButtonText: "Acepto"
        }).then(resp => {
            if(resp.isConfirmed){
                updateURL("");
                localStorage.removeItem("muwieriURL");
                Swal.fire({
                    title: "Datos eliminados",
                    text: "Los datos almacenados se han eliminado",
                    icon: "success"
                });
            }
        });
    }
    return (
        <header>
            <h1>Muwieri Reactive</h1>
            {Boolean(url) ? <button onClick={deleteCurrentServerData}>Cambiar de servidor</button> : null }
            <h1>{Boolean(login) ? login.name : "Invitado"}</h1>
        </header>
    );
};

export default Header;