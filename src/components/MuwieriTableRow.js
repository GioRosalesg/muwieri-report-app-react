const MuwieriTableRow = ({data}) => {
    return (
        <tr className={ data.no_alertas > 0 ? "registro-alerta" : ""}>
            <td>{data.id}</td>
            <td>{data.edad}</td>
            <td>{data.sexo}</td>
            <td>{data.edo_civ}</td>
            <td>{data.centro_salud}</td>
            <td>{data.c_suicida}</td>
            <td>{data.f_acceso}</td>
            <td>{data.no_sesiones}</td>
            <td>{data.no_alertas}</td>
            <td>{data.f_ultima_alerta}</td>
            <td><a href={data.report_url} rel="noreferrer" target="_blank" >Consultar Reporte</a></td>
        </tr>
    )
};

export default MuwieriTableRow;