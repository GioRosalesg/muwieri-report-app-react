import { Component }        from 'react';
import Header               from './components/Header';
import SetURL               from './components/SetURL';
import Login                from './components/Login';
import MuwieriTable         from './components/MuwieriTable';

class App extends Component {
  state = {
    url:          localStorage.getItem("muwieriURL") || "",
    login:        {},
    helpathData:  []
  }

  updateURL = (url) => {
    this.setState({
      url,
      login:        (url ? this.state.login : {}),
      helpathData:  []
    });
  }

  handleLogin = (login, helpathData) => {
    this.setState({
      login,
      helpathData
    });
  }

  render() {
    const {url, login, helpathData} = this.state;

    return(
      <>
       <Header 
        url={url} 
        login={login.user} 
        updateURL={this.updateURL} 
      />
       <div className="container">
         { !url && <SetURL url={url} updateURL={this.updateURL} />}
         { url  && !login.user && <Login url={url} handleLogin={this.handleLogin} />}
         { url  && login.user  && <MuwieriTable url={url} helpathData={helpathData} />}
       </div>
     </>
    )
  }
}

export default App;