const MuwieriRequests = {
    checkAPIURL: (host) => {
        return fetch(`${host}/ping.json`, {
            method: 'GET',
            mode: 'cors',
            credentials: 'include',
            headers: {
                "Accept": "application/json"
            }
        })
        .then(async resp => {
            if(resp.ok)
                return await resp.json();
            
            return {status: ""};
        })
        .then(
            data => {
                if(!Boolean(data.status))
                    return false;

                return true;
            }
        )
        .catch(() => false);
    },
    getHelpathData: ({host, user, pass}) => {
        return fetch(`${host}/reporte-app.json`, {
            method: "POST",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({user: {email: user, password: pass}})
        })
        .then(async resp => {
            if(resp.ok)
                return await resp.json();

            return {status: "failed", message: "Request failed"};
        })
        .catch(() => {
            return {status: "failed", message: "Failed making request"}
        });
    },
    getHelpathExcel: ({host}) => {
        return fetch(`${host}/reporte-app.xlsx`, {
            method: "POST",
            mode: "cors",
            credentials: 'include',
            headers: {
                "Accept": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                "Content-Type": "application/json"
            }
        })
        .then(async (resp) => {
            if(resp.ok)
                return {status: 200, data: await resp.blob()};

            return {status: "failed", message: "Request failed"};
        })
        .catch(() => {
            return {status: "failed", message: "Failed making request"}
        });
    }
};

export default MuwieriRequests;