import MuwieriImage from '../images/logo_muwieri.png';
const Logo = () => <img src={MuwieriImage} alt="Muwieri Logo" title="Muwieri Logo" className="main-logo" />

export default Logo;