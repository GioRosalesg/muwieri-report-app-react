module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: theme => ({
        'hero-pattern': "url(/src/images/puntos_fondo_v2.png)"
      }),
      colors: {
        primary: {
          DEFAULT:  "rgb(255, 144, 82)",
          accent:   "rgb(246, 115, 44)"
        },
        secondary: {
          DEFAULT:  "rgb(84, 221, 180)",
          accent:   "rgb(46, 218, 166)"
        }
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
